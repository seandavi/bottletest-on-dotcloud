Using this git repository to create a website running on dotcloud required creating an account and then:

    git clone ....
    cd bottletest-on-dotcloud
    dotcloud create bottletest
    # answer the questions, including "connecting" the current working directory
    dotcloud push

After completing these steps, the last line of the output should include the url for the live site that you can go and visit.  Simple ab output suggests about 50 hits/second is possible--not bad for free.

You'll note three special files.  The dotcloud.yml file contains the setup of the service and is pretty simple in this case.  This is also where you would set up other services such as a database.  The second is the requirements.txt and contains a list of the requirements to be installed (by easy_install) on the dotcloud instance.  The third is the wsgi.py file (must have that name) that must contain an instance of a wsgi application called "application".  
 