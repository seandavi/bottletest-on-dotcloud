import bottle
from bottle import route, run, template

@route('/')
@route('/hello/:name')
def index(name='World'):
    return template('<b>Hello {{name}}</b>!', name=name)

application = bottle.default_app()
